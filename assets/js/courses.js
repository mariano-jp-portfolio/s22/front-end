// This variable stores the isAdmin object in the user's local storage
let adminUser = localStorage.getItem("isAdmin");

// Cardfooter dynamically renders if the user is an admin or not
let cardFooter;

// Fetch request to all users
fetch('http://localhost:4000/api/courses')
.then(res => res.json())
.then(data => {
	// Log the data to check if it's alright to fetch the data from the server
	console.log(data); //for checking
	
	// courseData will store the data to be rendered
	let courseData;
	
	// Logic
	if (data.length < 1) {
		courseData = "No courses available.";
	} else {
		// To iterate the courses collection and display each course
		courseData = data.map(course => {
			console.log(course._id); //checks the make up of each element inside the courses collection
			
			// If the user is regular, display when the course was created
			if (adminUser === "false" || !adminUser) {
				cardFooter =
					`
						<a href="./course.html?courseId=${course._id}" value="{course._id}" class="btn btn-primary text-white btn-block editButton">
							Select Course
						</a>
					`;
			} else {
				// Admin user is given permission to edit/delete a course
				cardFooter =
				`
					<a href="./editCourse.html?courseId=${course._id}" value="{course._id}" class="btn btn-primary text-white btn-block editButton">
						Edit
					</a>
					
					<a href="./deleteCourse.html?courseId=${course._id}" value="{course._id}" class="btn btn-primary text-white btn-block dangerButton">
						Delete
					</a>
				`;
			}
			// After determining if a user is an admin or not, display the details of the courses to them
			return (
					`
						<div class="col-md-6 my-3">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">
										${course.name}
									</h5>
									
									<p class="card-text text-left">
										${course.description}
									</p>
									
									<p class="card-text text-right">
										${course.price}
									</p>
								</div>
								
								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
						</div>
					`
				)
		}).join(""); // Our data collection is an array that is why we use join with separator "" to return a new string
	}
	// Get the value of courseData and assign it as the #courseContainer's content
	let container = document.querySelector('#coursesContainer');
	
	container.innerHTML = courseData;
});

// Add modal - if a user is an admin, there will be a button to add a course
let modalButton = document.querySelector('#adminButton');

if (adminUser === "false" || !adminUser) {
	// if they are a regular user, don't show the modal
	modalButton.innerHTML = null;
} else {
	// display the button
	modalButton.innerHTML = 
	`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse" class="btn btn-block btn-primary">
				Add Course
			</a>
		</div>
	`;
}