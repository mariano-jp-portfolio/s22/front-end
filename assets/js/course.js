// window.location.search returns the query string part of the url
// console.log(window.location.search);

// create a variable for our course ID parameter
let params = new URLSearchParams(window.location.search);

// stores the retrieved variable
let courseId = params.get('courseId');

// token
let token = localStorage.getItem('token');


let courseName = document.querySelector('#courseName');
let courseDescription = document.querySelector('#courseDescription');
let coursePrice = document.querySelector('#coursePrice');
let enrollContainer = document.querySelector('#enrollContainer');

// Create a fetch request to display data
fetch(`http://localhost:4000/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	// console.log(data);
	// Render the data using innerHTML
	courseName.innerHTML = data.name;
	courseDescription.innerHTML = data.description;
	coursePrice.innerHTML = data.price;
	enrollContainer.innerHTML = 
		`
			<button id="enrollButton" class="btn btn-block btn-primary">
				Enroll
			</button>
		`;
		
	// Create logic for the enroll button POST method
	document.querySelector('#enrollButton').addEventListener('click', () => {	
		// insert the course
		fetch('http://localhost:4000/api/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			// Create a new course if successful
			if (data === true) {
				// redirect the user to the course page
				alert('Thank you for enrolling! See you!');
				window.location.replace("./course.html");
			} else {
				// redirect in creating course
				alert('Oops, something went wrong.')
			}
		})
	});
})